
### Html Tag

### Css Property

### Flex Box

### Bài tập về nhà
 

## Git

  #### Config

  Để kiểm tra lại thông tin mà mình đã config, dùng câu lệnh:

  ```Javscript
    $ git config --list
  ```

  #### Init

  Cậu lệnh Git này dùng để tạo mới 1 project trên máy. Tạo ở bất kỳ đâu tùy thích.

  ```Javscript
    $ git init
  ```

  Sau khi chạy câu lệnh này xong, 1 thư mục ẩn .git sẽ xuất hiện. Thư mục này chưa thông tin cấu hình của project Git.

  #### Clone

  Copy 1 project từ Local Repository đến 1 thư mục khác, hoặc từ server về máy tính của bạn.

  ```javascript
    $ git clone path
  ```

  Clone project từ server có thể sử dụng https, ssh.
  
    - https:
  
  ```javascript
    $ git clone username@server ...
  ```

    - ssh:
  
  ```javascript
    $ git clone ssh://username@server ...
  ```

  #### Branch

  Bạn có thể tạo ra nhiều branch khác nhau. Câu lệnh Git này dùng để kiểm tra branch hiện tại:

  ```javascript
    $ git branch
  ```

  Mặc định ban đầu là nhánh master - nhánh chính

  Để tạo mới 1 branch:

  ```javascript
    $ git branch <name_branch>
  ```

  #### Checkout

  Trước khi muốn thay đổi source code, điều đầu tiên bạn cần làm là checkout 1 branch:

  ```javascript
    $ git checkout <name_branch>
  ```

  Để tạo mới 1 branch và checkout luôn branch đó dùng lệnh:

  ```javascript
    $ git checkout -b <name_branch>
  ```

  Để quay lại branch master:

  ```javascript
    $ git checkout master
  ```

  #### Add

  Sau khi bạn thay đổi source code: thêm mới, sửa, xóa .. Bạn cần phải cập nhật lên Staging Area

  Cập nhật hết các files:

  ```javascript
    $ git add .
  ```

  Cập nhật các file với đuôi xác định

  ```javascript
    $ git add .<tên đuôi>
  ```

  #### Commit

  Thay đổi thông tin trên Local Repository:

  ```javascript
    $ git commit -m "nội dung"
  ```

  #### Push

  Sau commit thông tin chỉ được cập nhật lên Local Repository. Nếu muốn cập nhật lên server thì sử dụng câu lệnh:

  ```javascript
    $ git push origin <name_branch>
  ```

  #### Pull, Merge

## Html Tag

  * [https://www.w3schools.com/]

## Css Property

  ### Những vấn đề trong css hay gặp ở dự án thực tế:

  #### Center với text-align

  Khi các bạn làm việc với các khối hay hình ảnh có HTML đơn giản như sau, thì các bạn muốn cho hình ra giữa các bạn sẽ dùng display: block cho tấm hình rồi sau đó dùng margin: 0 auto.

  ```javascript
    <div class="image"><img src="demo.jpg"></div>
  ```

  ```javascript
    .image img {
      display: block; 
      margin: 0 auto;
    }
  ```

  Tuy nhiên img đó ko dùng display: block mà dùng display: inline-block thì margin: 0 auto sẽ ko còn tác dụng nữa

  #### Thiết lập max-width cho hình ảnh

  Đôi khi chúng ta code có sử dụng nhiều hình ảnh rất to, nhưng khung chứa thì rất nhỏ dù đã thiết lập chiều rộng hay độ cao mà hình ảnh không có thu vào mà vẫn to như ảnh gốc ban đầu. Thì lúc này các bạn nên xem lại hình ảnh có dùng max-width chưa .

  ```javascript
    img { max-width: 100%; }
  ```

  #### Fonts không hiển thị đúng

  Khi các bạn thiết lập dự án ban đầu, các bạn sẽ thường thiết lập font-family trên body chẳng hạn để áp dụng cho toàn trang, tuy nhiên các thẻ như input, textarea, select, button sẽ không nhận mà nó lấy dựa theo mặc định của trình duyệt. Để xử lý trường hợp này các bạn phải CSS thêm font-family cho các thẻ này

  ```javascript
    input,
      button,
      select,
      textarea {
        font-family: "some font", sans-serif;
      }
  ```

  #### Hình ảnh hiển thị bị méo

  Khi các bạn dùng thẻ img trong một khung nào đó chẳng hạn có kích thước là 50×50 mà hình của bạn là 135×275 thì lúc này hình của bạn sẽ hiển thị không được đẹp vì thế đừng quên sử dụng thuộc tính object-fit: cover để xử lý trường hợp này nhé.

  ```javascript
    object-fit: cover;
  ```

  #### Thẻ label không phải để chưng

  Khi các bạn dùng thẻ label thì sẽ có thuộc tính for="" đi kèm, ở trong for này giá trị truyền vào chính là id của một input nào đó ví dụ

  ```javascript
    <label for="email">Email</label>
    <input type="email" id="email"/>
  ```

  Lúc này khi nhấn vào thẻ label thì input với id tương ứng chỗ for sẽ được focus

  #### Xử lý khi liên kết dài hoặc chữ dài

  Chắc hẳn khi các bạn làm giao diện layout thì việc hiển thị nội dung ra là chuyện đương nhiên và nội dung này được nhập từ người dùng. Ví dụ họ nhập 1000 chữ a dính nhau thì lúc này giao diện của các bạn sẽ hiển thị scroll ngang và sẽ làm xấu giao diện chẳng hạn. Để xử lý trường hợp này các bạn chỉ cần dùng như sau, ví dụ là class .text.

  ```javascript
    .text{
      word-break: break-all;
    }
  ```

  #### Chú ý với button

  Khi các bạn dùng thẻ button thì ở trình duyệt như Chrome thì hiển thị bình thường nhưng ở Safari thì có màu nền làm xám. Để xử lý trường hợp này các bạn thiết lập màu nền mặc định là transparent là ok.

  ```javascript
    button{
      background-color: transparent;
    }
  ```

  #### Border bị mất 1 phần khi zoom

  Khi làm việc với các thẻ như input chúng ta thường code border: 1px solid #eee chẳng hạn, thì trên trình duyệt Chrome hay Safari lâu lâu sẽ bị mất border ở dưới hoặc bên trái, để fix trường hợp này mình đã Google và thấy nếu dùng 1px thì chúng ta có thể thay bằng thin

  ```javascript
    .box{
      border: thin solid #eee;
    }
  ```

  #### Hiển thị dấu … khi nội dung quá dài

  Khi các bạn chỉ muốn một tiêu đề nào đó chỉ hiển thị trên một dòng và nếu quá dài thì sẽ bị cắt đi và hiển thị dấu 3 chấm thì chúng ta có thể dùng như thế này

  ```javascript
    .title{
      white-space: nowrap;
      text-overflow: ellipse;
      overflow: hidden;
    }
  ```

  Tuy nhiên bạn không muốn hiển thị trên một dòng mà là 3 hoặc 4 dòng rồi mới có dấu 3 chấm thì ta cũng có một cách khác để xử lý trường hợp này

  ```javascript
    .title{
      display: -webkit-box;
      -webkit-line-clamp: 3; // số dòng muốn hiển thị
      -webkit-box-orient: vertical;
      text-overflow: ellipse;
      overflow: hidden;
    }
  ```

  #### Margin - Outer spacing

  Margin thường được sử dụng khi các bạn muốn tạo khoảng cách giữa phần tử này so với các phần tử khác. 
  
  ##### Vấn đề margin collapse

  Hiểu đơn giản là margin collapse xảy ra khi chúng ta có 2 phần tử nằm hàng dọc và có margin và một trong số chúng có margin lớn hơn cái còn lại. Trong trường hợp này thằng nào có margin lớn hơn sẽ được sử dụng và thằng còn lại sẽ bị bỏ qua

  Và để giải quyết vấn đề này thì người ta khuyến khích chỉ sử dụng một một hướng mà thôi ví dụ các bạn muốn khoảng cách giữa chúng là 80px thì chỉ cần dùng margin-bottom: 80px là đủ, tuy nhiên nếu code .element {margin-bottom: 80px} thì cái cuối cùng sẽ có khoảng trống dư thừa cho nên để fix cái này chúng ta sẽ dùng :not(:last-child)

  ```javascript
    .element:not(:last-child) {
      margin-bottom: 80px;
    }
  ```

  Một ví dụ khác nữa cũng khá thú vị đó là mối quan hệ giữa phần tử cha và con bên trong. Giả sử ta có HTML và CSS như sau

  ```javascript
  <div class="parent">
    <div class="child">I'm the child element</div>
  </div>
  ```

  Các bạn thấy rằng phần tử con có margin: 50px 0 nhưng lại không cách ra phía trên so với phần tử cha bao ngoài, đó là vì margin collapse của thằng cha gây ảnh hưởng lên thằng con. Để giải quyết trường hợp này thì chúng ta có thể CSS cho thằng con thành display: inline-block hoặc hay hơn là thiết lập cho thằng cha padding-top: 50px chẳng hạn là ok.

  ##### Negative Margin

  Tức là sử dụng số âm trong giá trị của margin. Nó thật sự hữu ích trong nhiều trường hợp giả sử mình có cấu trúc giao diện như hình với padding bao quanh, làm cho phần tử con bị thu vào.

  #### # Padding – Inner Spacing

  ##### Padding không hoạt động

  Có một điều quan trọng là padding top và padding-bottom sẽ không hoạt động nếu phần tử các bạn đang làm có display: inline như là thẻ <span> hay là thẻ <a> . Nếu padding được thêm vào nó sẽ không ảnh hưởng tới phần tử. Để giải quyết vấn đề này thì các bạn có thể dùng thuộc tính display với giá trị khác như là inline-block hoặc block là oke thôi.

  ```javascript
  .element span {
    display: inline-block;
    padding-top: 1rem;
    padding-bottom: 1rem;
  }
  ```

## Flex Box

## Bài tập về nhà:
