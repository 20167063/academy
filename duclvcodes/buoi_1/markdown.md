## Markup

```javscript
  1. Hình ảnh: 
    - logo

  2. Fonts
    - font-family: Exo2
      - Title: Semibold
      - Placeholder: Italic
      - Button Text, Link, Regular
    - font-size: 
      - Heading tag: 26px
      - Placeholder, Button Text, Link: 18px

  3. Icons: 

  4. Background Color:
    - Body: #e5e5e5;
    - Form: #fff;
    - Button: #d61f22;

  5. Color: 
    - Title, Placeholder: #909090
    - Link: #cc1d20;
    - Button Text: #fff;

  6. Width, Height
    - Form: 
      - Width: 660px
  
  7. Spacing
    - Title:
        - margin-bottom: 10px
    - Button:
        - margin-bottom: 20px
```

## HTML
Img:
  alt: sử dụng như 1 văn bản thay thế khi images không hiển thị

Heading tag: Tạo những đề mục hoặc tiêu đề 
  h1, h2 ..

Input
 - text
 - checkbox ..

Button: Tạo các nút có thể click
  - type
  - name
  - Onclick

a tag: Tạo ra những link liên kết
  - href: link đến địa chỉ website đích
  - target: có vài giá trị cho nó
    - _blank: chuyển link tab mới
    ..
  - title: tiêu đề cho đường link
  - Nó gồm 4 trạng thái:
    - link:
    - visited:
    - hover: 
    - active:

## CSS
Reset CSS: đưa toàn bộ giá trị liên quan tới Box Model về 0
```sh
  * {
    padding: 0;
    margin: 0;
    box-sizing: 0;
  }
```
Padding: Căn chỉnh nội dung bên trong 1 thành phần so với viền của nó
Margin: Tạo khoảng cách từ các thành phần ra phía bên ngoài thành phần đó
Tiền tố: webkit, moz, o, ms
Line-height: Thiết lập chiều cao giữa các dòng


## Bài về nhà:
```sh
  Markdown
  Box-sizing
  FlexBox: https://flexbox.help/
  Box-shadow: https://www.cssmatic.com/box-shadow
```

Rules:

1. Hoàn thiện các bài tập về nhà và gửi lại link github lên group trước 20h trước mỗi buổi học kế tiếp
2. Vì lý do cá nhân không làm và nộp được bài phải báo trước 2 ngày trước buổi học kế tiếp, bài chưa làm hôm đó sẽ nộp vào buổi sau.
3. Không nộp bài sẽ cần giải trình với thầy trong buổi học tiếp theo (Trong thực tế đi làm nếu chậm task sẽ ngồi giải trình với leader).

Thầy mong mọi người chấp hành tốt quy tắc lớp học.